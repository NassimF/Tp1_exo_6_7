# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 16:39:04 2020

@author: tp
"""

import unittest
from MyCalculator import SimpleCalculator as Scalc
import logging


class TestCalc(unittest.TestCase):

    def test_to_sum(self):
        self.assertEqual(Scalc.to_sum(10, 5), 15)
        self.assertEqual(Scalc.to_sum(-1, 1), 0)
        self.assertEqual(Scalc.to_sum(-1, -1), -2)
        with self.assertRaises(ValueError):
            Scalc.to_sum(10, 5.0)

    def test_subtract(self):
        self.assertEqual(Scalc.subtract(10, 5), 5)
        self.assertEqual(Scalc.subtract(-1, 1), -2)
        self.assertEqual(Scalc.subtract(-1, -1), 0)
        with self.assertRaises(ValueError):
            Scalc.subtract(10, 5.0)
            
    def test_multiply(self):
        self.assertEqual(Scalc.multiply(10, 5), 50)
        self.assertEqual(Scalc.multiply(-1, 1), -1)
        self.assertEqual(Scalc.multiply(-1, -1), 1)
        with self.assertRaises(ValueError):
            Scalc.multiply(10, 5.0)
    
    def test_divide(self):
        self.assertEqual(Scalc.divide(10, 5), 2)
        self.assertEqual(Scalc.divide(-1, 1), -1)
        self.assertEqual(Scalc.divide(-1, -1), 1)
        self.assertEqual(Scalc.divide(5, 2), 2.5)
        with self.assertRaises(ValueError):
            Scalc.divide(10, 5.0)
    
    def test_errors(self):
        logging.warning('If 4 errors occured related to instance and divison by 0 the test is succesfull')
        
    


if __name__ == "__main__":
    unittest.main()
