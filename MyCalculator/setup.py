#!/usr/bin/env python3
 
from setuptools import setup
 
setup(
    name='MyCalculator',
    version='1.0.0',
    plateformes = 'LINUX',
    packages = ['MyCalculator'],
    author='NassimF',
    description='App Calculatrice Simple',
    license='GNU GPL v3',
    keywords = ["soustraction", "somme", "division","multiplication"],
    python_requires ='>=3.4'
    )


























