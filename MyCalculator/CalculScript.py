# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:20:40 2020

@author: nassim.fouadh
"""
class SimpleCalculator:
    """
    Class calculatrice
    """
    @staticmethod
    def to_sum(number_1, number_2):
        """
        Methode pour calcul de somme
        Argument: fait la somme de "number_1" et "number_2"
        """
        if isinstance(number_1, int) and isinstance(number_2, int):
            return number_1 + number_2
        else:
             raise TypeError("Arguments given must be int")
             
    @staticmethod
    def substract(number_1, number_2):
        """
        Methode pour calcul de difference
        Argument: fait la soustraction entre "number_1" et "number_2"
        """
        if isinstance(number_1, int) and isinstance(number_2, int):
            return number_1 - number_2
        else:
             raise TypeError("Arguments given must be int")
             
    @staticmethod
    def multiply(number_1, number_2):
        """
	    Methode pour calcul de multiplication
	    Argument: fait la multiplication entre "number_1" et "number_2"
	    """
        if isinstance(number_1, int) and isinstance(number_2, int):
            return number_1 * number_2
        else:
             raise TypeError("Arguments given must be int")
             
    @staticmethod
    def divide(number_1, number_2):
        """
        Methode pour calcul de division
        Argument: fait la division entre "number_1" et "number_2"
        """
        if isinstance(number_1, int) and isinstance(number_2, int):
            try:
                return number_1 / number_2
            except ZeroDivisionError :
                raise ZeroDivisionError("Cannot divide by zero")        
        else:
             raise TypeError("Arguments given must be int")

if __name__ == "__main__":
    CALC = SimpleCalculator()
    print(CALC.divide(2, 1))